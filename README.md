# ublock-modal-windows

Blocks modal windows, like cookie consent.

In uBlock Origin, go to the tab Filter Lists, and at the bottom click on 
"Import..." and paste this:

```
https://gitlab.com/gasull/ublock-modal-windows/raw/master/rules.txt
```